FROM ubuntu:xenial

WORKDIR '/app'

EXPOSE 9080

ARG GODOT_VERSION=3.2.2

COPY ./ ./

RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    wget \
    unzip \
    python \
    python-openssl \
    && rm -rf /var/lib/apt/lists/*

RUN wget https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_linux_server.64.zip \
    && unzip Godot_v${GODOT_VERSION}-stable_linux_server.64.zip \
    && mv Godot_v${GODOT_VERSION}-stable_linux_server.64 /usr/local/bin/godot \
    && rm Godot_v${GODOT_VERSION}-stable_linux_server.64.zip

CMD ["godot", "--main-pack", "server.pck"]
